use std::collections::{hash_map, HashMap};
use std::hash::Hash;

#[derive(Debug, Clone)]
pub struct Counter<T> 
where T: Eq + PartialEq + Hash + Clone,
{
    inner: HashMap<T, usize>,
}

impl<T> Counter<T>
where T: Eq + PartialEq + Hash + Clone,
{
    // [[constructors]]

    pub fn new() -> Self {
        Counter {inner: HashMap::new(),}
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Counter {inner: HashMap::with_capacity(capacity),}
    }

    // [[single element actions]]

    pub fn insert(&mut self, key: T) {
        self.inner.entry(key).and_modify(|x| *x += 1).or_insert(1);
    }

    pub fn get(&self, key: &T) -> Option<&usize> {
        self.inner.get(key)
    }

    pub fn reset_elem(&mut self, key: T) {
        self.inner.entry(key).and_modify(|v| *v = 0);
    }

    pub fn remove(&mut self, key: &T) -> Option<usize> {
        self.inner.remove(key)
    }

    // [[full map actions]]

    pub fn clear(&mut self) {
        self.inner.clear();
    }

    /// reset the count of all items to zero
    pub fn reset(&mut self) {
        self.inner.values_mut().for_each(|v| *v = 0);
    }

    pub fn raw_map(self) -> HashMap<T, usize> {
        self.inner
    }

    pub fn keys(&self) -> hash_map::Keys<'_, T, usize> {
        self.inner.keys()
    }
    
    pub fn values(&self) -> hash_map::Values<'_, T, usize> {
        self.inner.values()
    }

    pub fn total(&self) -> usize {
        self.values().sum()
    }

    // [[Counter-to-Counter operations]]

    pub fn is_subset_of(&self, other: &Self) -> bool {
        self.inner.iter().all(|(k, v)| {
            match other.get(k) {
                // has to be there
                None => false,
                // has to be less than the other
                Some(ov) => {v < ov},
            }
        })
    }

    pub fn merge(&self, other: &Self) -> Self {
        // first allocation is the bigger one
        let (long, short) = if self.inner.len() > other.inner.len() {
            (self, other)
        } else {
            (other, self)
        };
        let mut inner = long.inner.clone();
        for (key, val) in short.inner.iter() {
            let key = (*key).clone();
            inner.entry(key)
                .and_modify(|v| *v += val)
                .or_insert(*val);
        }
        Counter{inner}
    }

    pub fn intersect(&self, other: &Self) -> Self {
        let mut inner = self.inner.clone();
        for (key, val) in other.inner.iter() {
            let key = (*key).clone();
            inner.entry(key)
                .and_modify(|v| *v = (*v).min(*val) )
                .or_insert(0);
        }
        Counter{inner}
    }
}

impl<T> Default for Counter<T>
where T: Eq + PartialEq + Hash + Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T> std::iter::FromIterator<T> for Counter<T>
where T: Eq + PartialEq + Hash + Clone,
{
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let mut out = Counter::with_capacity(16);
        for i in iter {
            out.insert(i);
        }
        out
    }
}
